Title: Changes with Mesa 22
Author: Timo Gurr <tgurr@exherbo.org>
Content-Type: text/plain
Posted: 2022-04-28
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: x11-dri/mesa[<22.0.0]

With Mesa 22 the classic drivers and infrastructure were removed. Specifically, intel,
nouveau-legacy and radeon-legacy are gone.

For intel the replacements are the various Gallium-based successors:

video_drivers: i915 - For Intel i915 (Gen3) GPUs
video_drivers: crocus - For Intel i965 to Haswell (Gen4 to Gen7) GPUs
video_drivers: iris - For Intel Broadwell/Braswell (Gen8) and newer GPUs

When looking up and picking a driver note that the GPU generations are not identical to
the CPU ones.

For more information read the descriptions of the options.
