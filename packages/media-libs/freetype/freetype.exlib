# Copyright 2007 Alexander Færøy <ahf@exherbo.org>
# Copyright 2010 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'freetype-2.3.7.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require meson

export_exlib_phases src_prepare src_compile src_configure src_install

SUMMARY="A portable font engine"
HOMEPAGE="https://www.${PN}.org"
DOWNLOADS="mirror://sourceforge/${PN}/${PN}2/${PV}/${PNV}.tar.xz
    utils? ( mirror://sourceforge/${PN}/${PN}-demos/${PV}/ft2demos-${PV}.tar.xz )"

REMOTE_IDS="sourceforge:${PN}"

UPSTREAM_RELEASE_NOTES="https://sourceforge.net/projects/${PN}/files/${PN}2/${PV}/"

LICENCES="|| ( FTL GPL-2 ) MIT ZLIB public-domain"
SLOT="2"
# harfbuzz option added to allow breaking a cycle between freetype and harfbuzz
MYOPTIONS="
    harfbuzz [[ description = [ improve auto-hinting of OpenType fonts ] ]]
    utils
    woff2 [[ description = [ Support decompression of Web Open Font Format2 streams ] ]]
"


DEPENDENCIES="
    build+run:
        app-arch/bzip2
        media-libs/libpng:=
        sys-libs/zlib
        virtual/pkg-config[>=0.24]
        harfbuzz? ( x11-libs/harfbuzz[>=2.0.0] )
        utils? (
            x11-libs/libX11
            x11-libs/qtbase:5
        )
        woff2? ( app-arch/brotli )
"

if ever at_least 2.12.0 ; then
    DEPENDENCIES+="
        build:
            dev-lang/python:*[>=3]
        build+run:
            utils? ( gnome-desktop/librsvg:2[>=2.46.0] )
    "
else
    DEPENDENCIES+="
        build:
            dev-lang/python:*
    "
fi

freetype_src_prepare() {
    meson_src_prepare

    local headerdir
    headerdir=include/${PN}/config

    enable_option() {
        edo sed -i -e "/#define ${1}/a #define ${1}" ${headerdir}/ftoption.h
    }
    disable_option() {
        edo sed -i -e "/#define ${1}/ { s:^:/*:; s:$:*/: }" ${headerdir}/ftoption.h
    }

    # See http://freetype.org/patents.html about potential patent infringement issues
    enable_option FT_CONFIG_OPTION_SUBPIXEL_RENDERING

    enable_option PCF_CONFIG_OPTION_LONG_FAMILY_NAMES

    if option utils; then
        # Make freetype available to freetype-demos as a subproject
        edo ln -s "${MESON_SOURCE}" "${WORKBASE}"/ft2demos-$(ever range 1-3)/subprojects/${PN}2
        edo mkdir "${WORKBASE}"/_ft2demos_build

    fi
}

# Adds a prefix to an option switch if utils is enabled
_add_optional_prefix() {
    if option utils ; then
        echo "freetype2:${1}"
    else
        echo "${1}"
    fi
}

freetype_src_configure() {
    # ft2demos (included with [utils]) builds freetype as a subproject, so
    # we need to prefix the options we want to pass to freetype.
    local myconf=(
        -D$(_add_optional_prefix bzip2)=enabled
        -D$(_add_optional_prefix png)=enabled
        -D$(_add_optional_prefix zlib)=$(ever at_least 2.12.0 && echo "system" || echo "enabled")
        $(meson_feature harfbuzz $(_add_optional_prefix harfbuzz))
        $(meson_feature woff2 $(_add_optional_prefix brotli))
    )

    if option utils ; then
        MESON_SOURCE="${WORKBASE}"/ft2demos-${PV}

        myconf+=(
            -Dfreetype2:default_library=shared
        )
    fi

    exmeson "${myconf[@]}"
}

freetype_src_compile() {
    meson_src_compile

    # gen_docs would build API documentation, but needs docwriter (+python3)
    # (https://pypi.org/project/docwriter/), which has annoying exact version
    # deps.
}

freetype_src_install() {
    meson_src_install

    if option utils ; then
        # if utils is enabled we build freetype as a subproject of ft2demos
        edo cp "${WORKBASE}"/${PNV}/builds/unix/freetype-config.in "${TEMP}"/freetype-config
    else
        edo cp builds/unix/freetype-config.in "${TEMP}"/freetype-config
    fi
    edo sed -i \
        -e '/%PKG_CONFIG%/s|--variable exec_prefix|--variable prefix|g' \
        -e "s|%PKG_CONFIG%|$(exhost --tool-prefix)pkg-config|g" \
        "${TEMP}"/freetype-config
    dobin "${TEMP}"/freetype-config
}

