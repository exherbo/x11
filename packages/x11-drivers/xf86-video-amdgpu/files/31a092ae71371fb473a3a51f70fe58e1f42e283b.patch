Source/Upstream: Yes, fixed in git master

From 31a092ae71371fb473a3a51f70fe58e1f42e283b Mon Sep 17 00:00:00 2001
From: Hamza Mahfooz <hamza.mahfooz@amd.com>
Date: Thu, 7 Sep 2023 08:15:14 -0400
Subject: [PATCH] Enable TearFree if it's set to auto and VariableRefresh is
 enabled

As of kernel commit 1ca67aba8d11 ("drm/amd/display: only accept async
flips for fast updates"), we reject async flips that used to be silently
degraded to vsync. However, this causes issues for VRR if TearFree isn't
enabled, in particular we have seen that VRR fails to engage on a
handful of tested applications and in one case we even observed an
application soft hang. So, if TearFree is set to auto we should enable
it if VariableRefresh is enabled and we should warn users if they try to
enable VariableRefresh while TearFree is disabled.

Signed-off-by: Hamza Mahfooz <hamza.mahfooz@amd.com>
---
 man/amdgpu.man        | 3 ++-
 src/amdgpu_kms.c      | 4 ++++
 src/drmmode_display.c | 1 +
 3 files changed, 7 insertions(+), 1 deletion(-)

diff --git a/man/amdgpu.man b/man/amdgpu.man
index 54b5bd4..4ec2074 100644
--- a/man/amdgpu.man
+++ b/man/amdgpu.man
@@ -81,7 +81,8 @@ on. If this option is set, the default value of the property is 'on' or 'off'
 accordingly. If this option isn't set, the default value of the property is
 .B auto,
 which means that TearFree is on for rotated outputs, outputs with RandR
-transforms applied and for RandR 1.4 secondary outputs, otherwise off.
+transforms applied, for RandR 1.4 secondary outputs and if 'VariableRefresh'
+is enabled, otherwise it's off.
 .TP
 .BI "Option \*qVariableRefresh\*q \*q" boolean \*q
 Enables support for enabling variable refresh on the Screen's CRTCs
diff --git a/src/amdgpu_kms.c b/src/amdgpu_kms.c
index 1f049c9..882dc33 100644
--- a/src/amdgpu_kms.c
+++ b/src/amdgpu_kms.c
@@ -1655,6 +1655,10 @@ Bool AMDGPUPreInit_KMS(ScrnInfoPtr pScrn, int flags)
 			from = xf86GetOptValBool(info->Options, OPTION_VARIABLE_REFRESH,
 						 &info->vrr_support) ? X_CONFIG : X_DEFAULT;
 
+			if (info->vrr_support && !info->tear_free)
+				xf86DrvMsg(pScrn->scrnIndex, X_WARNING,
+					   "Enabling VariableRefresh while TearFree is disabled can cause instability!\n");
+
 			xf86DrvMsg(pScrn->scrnIndex, from, "VariableRefresh: %sabled\n",
 				   info->vrr_support ? "en" : "dis");
 
diff --git a/src/drmmode_display.c b/src/drmmode_display.c
index 00247ac..8244b92 100644
--- a/src/drmmode_display.c
+++ b/src/drmmode_display.c
@@ -593,6 +593,7 @@ drmmode_crtc_update_tear_free(xf86CrtcPtr crtc)
 		    (drmmode_output->tear_free == 2 &&
 		     (crtc->scrn->pScreen->isGPU ||
 		      info->shadow_primary ||
+		      info->vrr_support ||
 		      crtc->transformPresent || crtc->rotation != RR_Rotate_0))) {
 			drmmode_crtc->tear_free = TRUE;
 			return;
-- 
GitLab

