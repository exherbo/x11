# Copyright 2012 Alex Elsayed <eternaleye@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require bash-completion
require github [ user=xkbcommon tag=xkbcommon-${PV} ] meson

export_exlib_phases src_prepare

SUMMARY="A common input handling library"
HOMEPAGE+=" https://xkbcommon.org"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    X
    doc
    wayland
    xkbregistry [[ description = [ A library to list available XKB models, layouts and variants for a given ruleset ] ]]
"

DEPENDENCIES="
    build:
        sys-devel/bison[>=2.3a]
        sys-devel/flex
        virtual/pkg-config[>=0.9.0]
        X? ( x11-proto/xorgproto )
        doc? ( app-doc/doxygen )
    build+run:
        dev-libs/icu:=
        X? ( x11-libs/libxcb[>=1.10] )
        wayland? (
            sys-libs/wayland[>=1.2.0]
            sys-libs/wayland-protocols[>=1.12]
        )
        xkbregistry? (
            dev-libs/libxml2:2.0
            x11-apps/xkeyboard-config
        )
"

MESON_SRC_CONFIGURE_PARAMS=(
    # xkbcli tool, no deps, small and needed for wayland anyway
    -Denable-tools=true
    -Dxkb-config-root=/usr/share/X11/xkb
)
MESON_SRC_CONFIGURE_OPTION_ENABLES=(
    'X x11'
    bash-completion
    'doc docs'
    wayland
    xkbregistry
)

libxkbcommon_src_prepare() {
    meson_src_prepare

    # Skip tests, which would need Xvfb
    edo sed -e "s|executable('test-x11.*|find_program('true'),|" \
        -i meson.build
}

