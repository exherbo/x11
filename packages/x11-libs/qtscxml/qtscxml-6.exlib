# Copyright 2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt cmake [ ninja=true ]

export_exlib_phases src_compile src_install

SUMMARY="Qt Cross-platform application framework: Scxml and StateMachine"
DESCRIPTION="
The Qt SCXML module provides classes for embedding state machines created
from State Chart XML (SCXML) files in Qt applications.
The State Machine framework provides classes for creating and executing
state graphs.
"
LICENCES="FDL-1.3 GPL-2 GPL-3 LGPL-3"
MYOPTIONS="
    examples
"

DEPENDENCIES="
    build+run:
        x11-libs/qtbase:${SLOT}[>=${PV}]
        x11-libs/qtdeclarative:${SLOT}[>=${PV}]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DFEATURE_qeventtransition:BOOL=TRUE
    -DFEATURE_scxml_ecmascriptdatamodel:BOOL=TRUE
    -DFEATURE_statemachine:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'examples QT_BUILD_EXAMPLES'
)

qtscxml-6_src_compile() {
    cmake_src_compile

    option doc && eninja docs
}

qtscxml-6_src_install() {
    cmake_src_install

    option doc && DESTDIR="${IMAGE}" eninja install_docs

    # Remove empty dir
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/lib/cmake/Qt6
}

