# Copyright 2013-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt cmake [ ninja=true ]
require llvm [ additional_packages=[ "dev-lang/clang" ] ]

export_exlib_phases src_configure src_compile src_install

SUMMARY="Qt Cross-platform application framework: QtTools"
DESCRIPTION="
Qt Assistant: A Tool for viewing the Qt documentation
Qt Designer: Designing and building graphical user interfaces in a visual editor
Qt Linguist: Contains tools for the roles typically involved in localizing applications
D-Bus Viewer: A tool to introspect D-Bus objects and messages.
"

MYOPTIONS="
    assistant [[ description = [ A tool for viewing on-line docs in Qt help format ] ]]
    designer [[ description = [ A tool for designing and building GUIs ] ]]
    distancefieldgenerator [[ description = [ Tool to generate font caches to optimize startup performance ] ]]
    examples
    gui [[ description = [ Build GUI tools ]
        presumed = true
    ]]
    linguist [[ description = [ Helps to translate text in Qt applications ] ]]

    ( assistant designer distancefieldgenerator linguist ) [[ *requires = gui ]]
    ( examples linguist ) [[ *requires = [ designer ] ]]
"

DEPENDENCIES="
    build+run:
        assistant? ( x11-libs/qtbase:${SLOT}[>=${PV}][sql][sqlite] )
        designer? ( app-arch/zstd[>=1.3] )
        gui? (
            x11-libs/qtbase:${SLOT}[>=${PV}][gui(+)]
            x11-libs/qtdeclarative:${SLOT}[>=${PV}][gui(+)]
        )
        !gui? (
            x11-libs/qtbase:${SLOT}[>=${PV}][?gui(+)]
            x11-libs/qtdeclarative:${SLOT}[>=${PV}][?gui(+)]
        )
"
# TODO: bundles qlitehtml
# NOTE: Has some "if(TARGET Qt::WebKitWidgets)" but it's not searched for

qttools-6_src_configure() {
    llvm_providers_set

    local myconf=(
        -DLLVM_INSTALL_DIR:PATH=/usr/$(exhost --target)/lib/llvm/$(llvm_get_abi)

        # Require for QDoc, which is needed to build qt*[doc] and other docs
        -DFEATURE_clang:BOOL=ON
        -DFEATURE_qtattributionsscanner:BOOL=ON
        -DFEATURE_qdoc:BOOL=ON
        -DFEATURE_qtplugininfo:BOOL=OFF

        $(cmake_option examples QT_BUILD_EXAMPLES)

        $(qt_cmake_feature assistant)
        $(qt_cmake_feature designer)
        $(qt_cmake_feature distancefieldgenerator)
        $(qt_cmake_feature linguist)
        $(qt_cmake_feature gui pixeltool)
        $(qt_cmake_feature gui qdbus)
        # The option exists but its dir isn't include with the build system
        #$(qt_cmake_feature gui qev)
        $(qt_cmake_feature gui qtdiag)
    )

    cmake_src_configure "${myconf[@]}"
}

qttools-6_src_compile() {
    if option examples && option linguist ; then
        # Create translation files arrowpad_{fr,nl}.ts, hellotr_la.ts
        eninja arrowpad_lupdate
        eninja hellotr_lupdate
    fi

    cmake_src_compile

    option doc && eninja docs
}

qttools-6_src_install() {
    cmake_src_install

    option doc && DESTDIR="${IMAGE}" eninja install_docs

    local host=$(exhost --target)
    # They collide with the versions from x11-libs/qttools:5
    local colliding_binaries=( lconvert lrelease lupdate qdoc )

    # TODO: We should probably on symlink user facing tools /usr/host/bin
    option assistant && colliding_binaries+=( assistant )
    option designer && colliding_binaries+=( designer )
    option distancefieldgenerator && colliding_binaries+=( qdistancefieldgenerator )
    option gui && colliding_binaries+=( pixeltool qdbus qdbusviewer qtdiag )
    option linguist && colliding_binaries+=( linguist )

    edo mkdir -p "${IMAGE}"/usr/${host}/bin
    for i in ${colliding_binaries[@]} ; do
        [[ -e "${IMAGE}"/usr/${host}/lib/qt6/bin/${i} ]] || die "/usr/${host}/lib/qt6/bin/${i} does not exist in ${IMAGE}"
        dosym /usr/${host}/lib/qt6/bin/${i} /usr/${host}/bin/${i}-qt6
    done
}

