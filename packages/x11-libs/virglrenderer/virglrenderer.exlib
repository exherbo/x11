# Copyright 2016-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    require gitlab [ prefix=https://gitlab.freedesktop.org user=virgl branch=main ]
else
    require gitlab [ prefix=https://gitlab.freedesktop.org user=virgl tag=${PV} new_download_scheme=true ]
fi

require meson
require option-renames [ renames=[ 'va vaapi' ] ]

export_exlib_phases src_configure

SUMMARY="Virgl rendering library"
DESCRIPTION="
The virgil3d rendering library is used by QEMU to implement 3D GPU support for the virtio GPU.
"
HOMEPAGE+=" https://virgil3d.github.io"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    vaapi [[ description = [ Adds support for hardware video acceleration using the Video Acceleration API ] ]]
    vulkan
    X
"

# Tests try to access /dev/dri/* resulting in access violations in sydbox
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/python:*
        virtual/pkg-config[>=0.9.0]
        x11-utils/util-macros[>=1.8]
    build+run:
        dev-libs/libepoxy[X?][>=1.5.4]
        x11-dri/libdrm[>=2.4.50]
        x11-dri/mesa[>=18.0.0] [[ note = [ gbm ] ]]
        vaapi? ( x11-libs/libva )
        vulkan? ( sys-libs/vulkan-loader )
        X? ( x11-libs/libX11 )
    test:
        dev-libs/check[>=0.9.4]
"

virglrenderer_src_configure() {
    local PLATFORMS=( egl )
    option X && PLATFORMS+=( glx )

    local meson_args=(
        -Dcheck-gl-errors=true
        -Ddrm=enabled
        -Dfuzzer=false
        -Dminigbm_allocation=false
        -Dplatforms=$(IFS=, ; echo "${PLATFORMS[*]}")
        -Drender-server-worker=process
        -Dtracing=none
        -Dunstable-apis=false
        -Dvalgrind=false
        -Dvenus-validate=false
        $(meson_switch vaapi video)
        $(meson_switch vulkan venus)
        $(expecting_tests '-Dtest=true' '-Dtests=false')
    )

    if ever is_scm ; then
        meson_args+=(
            -Ddrm-renderers=""
        )
    else
        meson_args+=(
            -Ddrm-msm-experimental=false
        )
    fi

    exmeson "${meson_args[@]}"
}

