# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require xorg [ suffix=tar.xz ] meson

SUMMARY="Combined X.Org X11 Protocol headers"

LICENCES="
    as-is [[ note = [ presentproto,dri3proto,xwaylandproto ] ]]
    MIT [[ note = [ compositeproto,damageproto,dmxproto,dri2proto,fixesproto,fontsproto,
        glproto,inputproto,kbproto,printproto,randrproto,renderproto,trapproto,xf86driproto,xextproto ] ]]
    ISC [[ note = [ recordproto,trapproto ] ]]
    BSD-2 [[ note = [ fontcacheproto ] ]]
    X11 [[ note = [ bigreqsproto,evieext,resourceproto,scrnsaverproto,videoproto,
    xcmiscproto,xineramaproto,xf86bigfontproto,xf86dgaproto,xf86miscproto,xf86vidmodeproto,xproto ] ]]
"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    test:
        dev-python/libevdev
    run:
        (
            !x11-proto/bigreqsproto
            !x11-proto/compositeproto
            !x11-proto/damageproto
            !x11-proto/dmxproto
            !x11-proto/dri2proto
            !x11-proto/dri3proto
            !x11-proto/evieext
            !x11-proto/fixesproto
            !x11-proto/fontcacheproto
            !x11-proto/fontsproto
            !x11-proto/glproto
            !x11-proto/inputproto
            !x11-proto/kbproto
            !x11-proto/presentproto
            !x11-proto/printproto
            !x11-proto/randrproto
            !x11-proto/recordproto
            !x11-proto/renderproto
            !x11-proto/resourceproto
            !x11-proto/scrnsaverproto
            !x11-proto/trapproto
            !x11-proto/videoproto
            !x11-proto/xcmiscproto
            !x11-proto/xextproto
            !x11-proto/xf86bigfontproto
            !x11-proto/xf86dgaproto
            !x11-proto/xf86driproto
            !x11-proto/xf86miscproto
            !x11-proto/xf86rushproto
            !x11-proto/xf86vidmodeproto
            !x11-proto/xineramaproto
            !x11-proto/xproto
        ) [[
            *description = [ Old *proto packages replaced by the unified xorgproto package ]
            *resolution = uninstall-blocked-before
        ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    # Also build legacy protos
    -Dlegacy=true
    # Used to install .pc files, which would otherwise land in
    # /usr/share/pkgconfig, but contain arch-specific paths.
    --datadir="/usr/$(exhost --target)/lib"
)

src_install() {
    meson_src_install

    # upstream moved these to legacy in 2019.2 which we still enable and additionally
    # XKBgeom.h is now shipped with libX11 >= 1.6.9 and vldXvMC.h with libXvMC >= 1.0.12
    # so we need to get rid of them here until we disable shipping legacy headers.
    edo rm "${IMAGE}"/usr/$(exhost --target)/include/X11/extensions/{XKBgeom.h,vldXvMC.h}
}

